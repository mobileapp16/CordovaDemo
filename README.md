Cordova Demo Project
---

__Prerequisites:__
 - Node.js
 - Cordova
 - Gulp

__Install Node.js:__

download it from [nodejs.org](https://nodejs.org/en/download/)

__Install Cordova:__

```
npm install -g cordova
```

__Install Gulp:__

```
npm install -g gulp
```

__Installa development tools:__

```
npm install
```

__Prepare www folder:__

```
gulp clean
gulp
```

__Add platforms:__

```
cordova platform add android
...
```