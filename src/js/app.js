/*globals ko, localStorage, navigator, window */

(function () {
    "use strict";

    var toDoRepository = {
        load: function () {
            return JSON.parse(localStorage.getItem('todos') || '[{"text":"Hello"}]');
        },
        add: function (todo) {
            var todos = this.load();
            todos.unshift(todo);
            this.save(todos);
        },
        save: function (todos) {
            localStorage.setItem('todos', JSON.stringify(todos));
        }
    };

    function ToDoListViewModel() {
        var self = this;

        self.todos = ko.observableArray(toDoRepository.load());

        self.removeToDo = function () {
            var todo = this;
            navigator.notification.confirm(
                'Are you sure you want to delete this todo?',
                function (button) {
                    if (button === 1) {
                        self.todos.remove(todo);
                        toDoRepository.save(self.todos());
                    }
                },
                'Delete Todo',
                ['Yes', 'No']
            );
            return false;
        };
    }

    function AddToDoViewModel() {
        var self = this;

        self.text = ko.observable('');

        self.addToDo = function () {
            toDoRepository.add({
                text: self.text()
            });
            window.history.back();
        };
    }

    ko.components.register('todo-list', {
        template: {element: 'todo-list'},
        viewModel: ToDoListViewModel
    });
    ko.components.register('add-todo', {
        template: {element: 'add-todo'},
        viewModel: AddToDoViewModel
    });

    ko.router.route('/', 'todo-list');
    ko.router.route('/new', 'add-todo');
    ko.router.start({hashbang: true, basePath: window.location.pathname});
    ko.applyBindings();
}());
