/*jslint node: true */
"use strict";

var gulp = require('gulp'),
    clean = require('gulp-clean'),
    rename = require('gulp-rename');

gulp.task('clean', function () {
    return gulp.src('./www/*', {read: false}).pipe(clean());
});

gulp.task('default', function () {
    gulp.src('./src/index.html').pipe(gulp.dest('./www'));
    gulp.src('./src/js/**').pipe(gulp.dest('./www/js'));
    gulp.src('./node_modules/jquery/dist/jquery.min.js').pipe(rename('jquery.js')).pipe(gulp.dest('./www/lib'));
    gulp.src('./node_modules/materialize-css/dist/js/materialize.min.js').pipe(rename('materialize.js')).pipe(gulp.dest('./www/lib'));
    gulp.src('./node_modules/materialize-css/dist/css/materialize.min.css').pipe(rename('materialize.css')).pipe(gulp.dest('./www/css'));
    gulp.src('./node_modules/materialize-css/dist/font/**').pipe(gulp.dest('./www/font'));
    gulp.src('./node_modules/knockout/build/output/knockout-latest.js').pipe(rename('knockout.js')).pipe(gulp.dest('./www/lib'));
    gulp.src('./node_modules/ko-component-router/dist/ko-component-router.min.js').pipe(rename('ko-component-router.js')).pipe(gulp.dest('./www/lib'));
});
